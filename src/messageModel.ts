export interface MessageModel {
    id: string,
    senderId: string,
    text: string,
    liked: boolean,
    createdAt: Date,
    editedAt: Date | null
}
