import sendIcon from "../../../icons/send.svg";
import "./MessagerInput.css";
import {useCallback, useEffect, useState} from "react";
import TextareaAutosize from "react-textarea-autosize";

interface MessageInputProps {
    send: (text: string) => void,
    defaultValue: string,
    placeholder: string
}

export function MessageInput(props: MessageInputProps) {

    const [text, setText] = useState(props.defaultValue);

    useEffect(() => {
        setText(props.defaultValue);
    }, [props]);

    const sendIfNotEmpty = useCallback(() => {
        if (text.trim().length !== 0) {
            props.send(text);
            setText("");
        }
    }, [text, setText, props]);

    return (
        <div className="message-input">
            <TextareaAutosize
                className="message-input-text"
                placeholder={props.placeholder}
                value={text}
                minRows={1}
                maxRows={5}
                onChange={event => setText(event.target.value)}
                onKeyDown={event => {
                    if (event.key === "Enter" && !event.shiftKey) {
                        sendIfNotEmpty();
                        event.preventDefault();
                    }
                }}
            />
            <button className="message-input-button" onClick={sendIfNotEmpty}>
                <img className="icon" width={25} height={25} src={sendIcon} alt="Send"/>
            </button>
        </div>
    );

}

MessageInput.defaultProps = {
    defaultValue: "",
    placeholder: "Write a message..."
}
