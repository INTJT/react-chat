import {ChatModel} from "../../../chatModel";
import _ from "lodash";
import {DayGroup} from "./day-group/DayGroup";
import {ChatTools} from "../Chat";

interface MessageListProps {
    chat: ChatModel,
    tools: ChatTools
}

export function MessageList(props: MessageListProps) {

    const days = Object.entries(_.groupBy(props.chat.messages, message => message.createdAt.toDateString()));

    return (
        <div className="message-list">
            {days.map(([day, messages]) => <DayGroup
                key={day}
                chat={props.chat}
                tools={props.tools}
                day={new Date(day)}
                messages={messages}
            />)}
        </div>
    );

}