import {ChatModel} from "../../../../chatModel";
import {MessageModel} from "../../../../messageModel";
import moment from "moment";
import {ME, User} from "../../../../user";
import {ChatTools} from "../../Chat";
import {Message} from "../message/Message";
import {OwnMessage} from "../message/OwnMessage";
import "./DayGroup.css";

//because no context...
interface MessageDividerProps {
    chat: ChatModel,
    day: Date,
    messages: MessageModel[],
    tools: ChatTools
}

function dateFormat(date: Date): string {
    const momentDate = moment(date);
    const today = moment();
    if(momentDate.isSame(today, "day")) return "Today";
    if(momentDate.isSame(today.clone().subtract(1, "day").startOf("day"))) return "Yesterday";
    else return momentDate.format("dddd, DD MMMM");
}

export function DayGroup(props: MessageDividerProps) {
    return (
        <div className="day-group">
            <div className="messages-divider">{dateFormat(props.day)}</div>
            {
                props.messages.map(message => {
                    if (message.senderId === ME.id) {
                        return (
                            <OwnMessage
                                key = {message.id}
                                message={message}
                                setEditableMessage={props.tools.setEditableMessage}
                                deleteMessage={props.tools.deleteMessage}
                            />
                        );
                    } else {
                        return (
                            <Message
                                key = {message.id}
                                message={message}
                                sender={props.chat.users.get(message.senderId) as User}
                                likeMessage={props.tools.likeMessage}
                            />
                        )
                    }
                })
            }
        </div>
    );
}
