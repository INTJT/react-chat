import {MessageModel} from "../../../../messageModel";
import {User} from "../../../../user";
import moment from "moment";
import "./Message.css";
import likeIcon from "../../../../icons/like.svg";

export interface MessageProps {
    message: MessageModel
}

interface AnotherMessageProps extends MessageProps {
    sender: User;
    likeMessage: (id: string) => void
}

export function Message(props: AnotherMessageProps) {
    return (
        <div className="message">
            <img className="message-user-avatar" src={props.sender.avatar} width={50} height={50} alt=""/>
            <div className="message-body">
                <h1 className="message-user-name one-line">{props.sender.name}</h1>
                <div className="message-text">{props.message.text}</div>
                <button
                    className={props.message.liked ? "message-liked" : "message-like"}
                    onClick={() => props.likeMessage(props.message.id)}
                >
                    <img className="icon" src={likeIcon} width={20} height={20} alt="" />
                </button>
                { props.message.editedAt ? <div className="message-edited one-line">edited</div> : null }
                <time className="message-time one-line">{moment(props.message.createdAt).format("HH:mm")}</time>
            </div>
        </div>
    );
}
