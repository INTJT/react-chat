import {MessageProps} from "./Message";
import moment from "moment";
import editIcon from "../../../../icons/edit.svg";
import deleteIcon from "../../../../icons/delete.svg";

interface OwnMessageProps extends MessageProps {
    setEditableMessage: (id: string) => void,
    deleteMessage: (id: string) => void
}

export function OwnMessage(props: OwnMessageProps) {
    return (
        <div className="own-message">
            <div className="message-buttons">
                <button
                    className="message-edit"
                    onClick={() => props.setEditableMessage(props.message.id)}
                >
                    <img className="icon" src={editIcon} width={15} height={15} alt="edit"/>
                </button>
                <button
                    className="message-delete"
                    onClick={() => props.deleteMessage(props.message.id)}
                >
                    <img className="icon" src={deleteIcon} width={15} height={15} alt="edit"/>
                </button>
            </div>
            <div className="message-body">
                <div className="message-text">{props.message.text}</div>
                { props.message.editedAt ? <div className="message-edited one-line">edited</div> : null }
                <time className="message-time one-line">{moment(props.message.createdAt).format("HH:mm")}</time>
            </div>
        </div>
    );
}