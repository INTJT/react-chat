import {useCallback, useEffect, useState} from "react";
import {MessageModel} from "../../messageModel";
import {ChatModel} from "../../chatModel";
import {ME, User} from "../../user";
import {Preloader} from "../preloader/Preloader";
import {Header} from "./header/Header";
import {MessageInput} from "./message-input/MessageInput";
import "./Chat.css";
import {NotFound} from "./NotFound";
import {Empty} from "./Empty";
import {MessageList} from "./message-list/MessageList";
import {v4} from "uuid";

interface ChatProps {
    url: string
}

export enum ChatStatus {
    Loading = "LOADING",
    NotFound = "NOT_FOUND",
    Messages = "MESSAGES"
}

type ChatState = { status: ChatStatus.NotFound | ChatStatus.Loading } | {
    status: ChatStatus.Messages,
    editable: MessageModel | null,
    chat: ChatModel
};

export interface ChatTools {
    setEditableMessage: (id: string) => void;
    deleteMessage: (id: string) => void;
    likeMessage: (id: string) => void
}

export function Chat(props: ChatProps) {

    const [state, setState] = useState({
        status: ChatStatus.Loading
    } as ChatState);

    //fetch messages
    useEffect(() => {

        interface RawMessage {
            id: string,
            userId: string,
            user: string,
            avatar: string,
            text: string,
            createdAt: string
            editedAt: string
        }

        function messageMapper(raw: RawMessage): MessageModel {
            return {
                id: raw.id,
                senderId: raw.userId,
                text: raw.text,
                liked: false,
                createdAt: new Date(raw.createdAt),
                editedAt: raw.editedAt ? new Date(raw.editedAt) : null
            }
        }

        (async function() {
            try {

                const response = await fetch(props.url);
                const rawMessages: RawMessage[] = await response.json();
                const users: Map<string, User> = new Map();
                rawMessages.forEach(raw => {
                    if(!users.has(raw.userId)) users.set(raw.userId, {
                        id: raw.userId,
                        name: raw.user,
                        avatar: raw.avatar
                    });
                });
                const messages: MessageModel[] = rawMessages.map(messageMapper);

                setState({
                    status: ChatStatus.Messages,
                    chat: {
                        name: "Chat",
                        users: users,
                        messages
                    },
                    editable: null
                });

            }
            catch {
                setState({
                    status: ChatStatus.NotFound
                });
            }
        } ())

    }, [props.url]);

    const findMessage = useCallback((id: string): MessageModel | null => {
       if(state.status === ChatStatus.Messages) {
           return state.chat.messages.find(message => message.id === id) || null;
       }
       else return null;
    }, [state]);

    const updateMessage = useCallback((oldMessage: MessageModel, newMessage: MessageModel | null): void => {
        if(state.status === ChatStatus.Messages) {

            const messages = [...state.chat.messages];
            const index = state.chat.messages.indexOf(oldMessage);

            if(index !== -1) {

                if(newMessage === null) messages.splice(index, 1);
                else messages[index] = newMessage;

                setState({
                    ...state,
                    editable: oldMessage !== state.editable ? state.editable : null,
                    chat: {
                        ...state.chat,
                        messages
                    }
                });

            }

        }
    }, [state]);

    const addMessage = useCallback((text: string) => {
        if(state.status === ChatStatus.Messages
            && text.trim() !== "") {

            state.chat.users.set(ME.id, ME);

            const newMessage: MessageModel = {
                id: v4(),
                senderId: ME.id,
                text,
                liked: false,
                createdAt: new Date(),
                editedAt: null
            }

            setState({
                ...state,
                chat: {
                    ...state.chat,
                    messages: [...state.chat.messages, newMessage]
                }
            });

        }
    }, [state, setState]);

    const setEditableMessage = useCallback((id: string | null) => {
        if(state.status === ChatStatus.Messages) {
            setState({
                ...state,
                editable: id === null ? null : findMessage(id)
            });
        }
    }, [state, setState, findMessage]);

    const editMessage = useCallback((newText: string) => {
        if(state.status === ChatStatus.Messages
            && state.editable
            && state.editable.text !== newText
            && newText.trim() !== "") {
            updateMessage(state.editable, {
                ...state.editable,
                text: newText,
                editedAt: new Date()
            });
        }
    }, [state, updateMessage]);

    const deleteMessage = useCallback((id: string) => {
        const message = findMessage(id);
        if(message != null) updateMessage(message, null);
    }, [findMessage, updateMessage]);

    const likeMessage = useCallback((id: string) => {
        if(state.status === ChatStatus.Messages) {
            const message = findMessage(id);
            if(message) {
                updateMessage(message, {
                    ...message,
                    liked: !message.liked
                });
            }
        }
    }, [updateMessage, state, findMessage]);

    switch (state.status) {

        case ChatStatus.Loading:
            return (
                <div className="chat loading">
                    <Preloader />
                </div>
            );

        case ChatStatus.NotFound:
            return (
                <div className="chat">
                    <NotFound />
                </div>
            );

        case ChatStatus.Messages:

            const chatClasses = ["chat"];
            const isEmpty = state.chat.messages.length === 0;
            if(isEmpty) chatClasses.push("empty");

            return (
                <div className={chatClasses.join(" ")}>
                    <Header chat={state.chat} />
                    <div className="main">
                        {
                            !isEmpty ?
                                <MessageList
                                    chat={state.chat}
                                    tools={{
                                        setEditableMessage,
                                        deleteMessage,
                                        likeMessage
                                    }}
                                />:
                                <Empty />
                        }
                        {
                            state.editable?
                                <button className="chat-cancel" onClick={() => setEditableMessage(null)}>cancel editing</button>:
                                null
                        }
                    </div>
                    {
                        state.editable ?
                            <MessageInput send={editMessage} defaultValue={state.editable.text} placeholder="Edit a message..." />:
                            <MessageInput send={addMessage} placeholder="Write a message..." />
                    }
                </div>
            );

    }

}

Chat.defaultProps = {
    url: "https://edikdolynskyi.github.io/react_sources/messages.json",
}
