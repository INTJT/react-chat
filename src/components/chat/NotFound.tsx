import notFoundIcon from "../../icons/not-found.svg";

export function NotFound() {
    return (
        <div className="not-found centered">
            <div>
                <img src={notFoundIcon} width={100} height={100} alt=""/>
            </div>
            <h1>Chat not found</h1>
        </div>
    );
}
