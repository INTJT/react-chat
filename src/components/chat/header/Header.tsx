import {ChatModel} from "../../../chatModel";
import moment from "moment";
import usersIcon from "../../../icons/users.svg";
import messageIcon from "../../../icons/message.svg";
import "./Header.css";

interface ChatProps {
    chat: ChatModel
}

export function Header(props: ChatProps) {

    const lastMessage = props.chat.messages[props.chat.messages.length - 1];
    const usersCount = new Set(props.chat.messages.map(message => message.senderId)).size;

    return (
        <div className="header">
            <h1 className="header-title one-line">{props.chat.name}</h1>
            <div className="one-line header-users">
                <img className="icon" width={20} height={20} src={usersIcon} alt=""/>
                <span className="header-users-count">{usersCount}</span> members
            </div>
            <div className="one-line header-messages">
                <img className="icon" width={20} height={20} src={messageIcon} alt=""/>
                <span className="header-messages-count">{props.chat.messages.length}</span> messages
            </div>
            {lastMessage ? (
                <div className="one-line header-date">
                    last message
                    at <time className="header-last-message-date">{moment(lastMessage.createdAt).format("DD.MM.yyyy HH:mm")}</time>
                </div>
            ) : null}
        </div>
    );

}
