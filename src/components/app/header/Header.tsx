import "./Header.css";
import chatIcon from "../../../icons/chat.svg";

export function Header() {
    return (
        <header>
            <h1>
                <img className="icon" src={chatIcon} width={25} height={25} alt=""/> Another one chat
            </h1>
        </header>
    );
}
