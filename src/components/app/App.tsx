import {Chat} from "../chat/Chat";
import {Header} from "./header/Header";
import './App.css';

function App() {
  return (
    <div className="app">
        <Header />
        <Chat />
    </div>
  );
}

export default App;
