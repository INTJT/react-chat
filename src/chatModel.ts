import {MessageModel} from "./messageModel";
import {User} from "./user";

export interface ChatModel {
    name: string,
    users: Map<string, User>,
    messages: MessageModel[]
}
