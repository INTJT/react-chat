export interface User {
    id: string,
    name: string,
    avatar: string,
}

export const ME: User = {
    id: "5328dba1-1b8f-11e8-9629-11037",
    name: "XXX",
    avatar: "https://pbs.twimg.com/profile_images/1210618202457292802/lt9KD2lt.jpg"
}
